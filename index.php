<!DOCTYPE html>
<html lang="en">

    <? include_once( "config.php" ); ?>
    <? include_once( "backend.php" ); ?>

  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <meta name="description" content="">
    <meta name="author" content="">
    <!--<link rel="icon" href="favicon.png">-->

    <link href="page-content/css/style.css" rel="stylesheet">
    <title> <?=$_GET["album"]?> - <?= $options['sitename']?> </title>
      
  </head>

  <body>
      
      <div class="view-container">
          <div class="main-view">
                <div class="pushed">
                    <section class="photos">

                    <? if ( sizeof( $photos ) > 0 ) { ?>
                    <h2>Photos</h2>
                    <ul class="cf photo-gallery">
                        <? foreach( $photos as $key => $photo ) { 
                            if ( strpos( $photo["name"], "thumbnail" ) === false )
                            {   
                            ?>
                            <li>
                                <a href="<?=$photo["path"]?>">
                                <? if ( $photo["thumbnail"] != "" ) { ?>
                                <img src="<?=$photo["thumbnail"]?>" title="<?=$photo["name"]?>">
                                <? } else { ?>
                                <img src="<?=$photo["path"]?>" title="<?=$photo["name"]?>">
                                <? } ?>
                                </a>
                                <span class="image-label">
                                    <? if ( strlen( $photo["name"] ) < 15 ) { ?>
                                    <?=$photo["name"]?>
                                    <? } else { ?>
                                    <?=substr( $photo["name"], 0, 10 ) . "..." ?>
                                    <? } ?>
                                </span>
                            </li>
                        <? 
                            } // if not thumbnail
                        } // foreach photo ?>
                    </ul>
                    <? } ?>
                    
                    
                    <? if ( sizeof( $videos ) > 0 ) { ?>
                    <h2>Videos</h2>
                    <ul>
                        <? foreach( $videos as $key => $video ) { ?>
                            <li><a href="<?=$video["path"]?>"><?=$video["name"]?></a></li>
                        <? } ?>
                    </ul>
                    <? } ?>
                </section>
                </div>
          </div>
          
          <div class="sidebar">
                <header>
                  <h1> <?= $options['sitename']?> </h1>
                </header>
                <section class="albums">
                    <h2>Albums</h2>

                    <ul>
                    <? foreach ( $albums as $key => $album ) { ?>
                        <li><a href="?album=<?=$album["name"]?>"><?=$album["name"]?> (<?=$album["file_count"]?>)</a></li>
                    <? } ?>
                    </ul>
                </section>
                <section class="repository">
                    <p>Dadem Gallery by Rachel Singh<br>Available on <a href="https://rejcx@bitbucket.org/rejcx/dadem-photo-gallery.git">BitBucket</a></p>
                </section>
          </div>
      </div>
      
  </body>
</html>
