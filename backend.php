<?
    function GetImageCount( $directory )
    {
        $files = scandir( $directory );
        unset( $files[1] ); // Remove ..
        unset( $files[0] ); // Remove .

        $fileCount = 0;
        
        foreach ( $files as $key => $file ) {
            if ( strpos( strtolower( $file ), "thumbnail" ) !== false )
            {
                continue;
            }
            $fileCount++;
        }
        
        return $fileCount;
    }

    $directories = glob($somePath . 'gallery/*' , GLOB_ONLYDIR);
    $albums = array();

    foreach ( $directories as $key => $dir ) {
        $fi = new FilesystemIterator( $dir , FilesystemIterator::SKIP_DOTS);
        
        array_push( $albums, array( "path" => $dir, "name" => str_replace( "gallery/", "", $dir ), "file_count" => GetImageCount( $dir ) ) );
    }

    $photos = array();
    $videos = array();
    if ( isset( $_GET["album"] ) ) {
        $pathPre = "gallery/" . $_GET["album"] . "/";
        $files = scandir( $pathPre );
        
        unset( $files[1] ); // Remove ..
        unset( $files[0] ); // Remove .

        foreach ( $files as $key => $file ) {
            if ( strpos( strtolower( $file ), ".mp4" ) !== false ) {
                array_push( $videos, array( "name" => $file, "path" => $pathPre . $file ) );
            }
            else if ( strpos( strtolower( $file ), "thumbnail" ) !== false ) {
                continue;
            }
            else {
                $thumbnail = "";
                if ( file_exists( $pathPre . "thumbnail_" . $file ) ) {
                    $thumbnail = $pathPre . "thumbnail_" . $file;
                }
                array_push( $photos, array( "name" => $file, "path" => $pathPre . $file, "thumbnail" => $thumbnail ) );
            }
        }
    }
?>

